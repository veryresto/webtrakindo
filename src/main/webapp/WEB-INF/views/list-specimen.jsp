<%@ include file="common/header.jspf"%>
<div class="directionRow">
	<div>
		<%@ include file="common/navigation.jspf"%>
	</div >
	<table class="table table-striped tableData addTodo">
		<caption>
			<div>
			<spring:message code="specimen.caption" />
			<a class="btn btn-success marginLeft20" href="/add-specimen">Add</a>
		</div>
		</caption>
		
		<thead>
			<tr>
				<th>Tanggal / Waktu Simpan</th>
				<th>Deskripsi</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${specimen}" var="specimen">
				<tr>
					<td><fmt:formatDate pattern="dd/MM/yyyy hh:mm:ss"
							value="${specimen.targetDate}" /></td>
					<td>${specimen.desc}</td>				
				</tr>
			</c:forEach>
		</tbody>
	</table>


</div>

<%@ include file="common/footer.jspf"%>

