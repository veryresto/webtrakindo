<%@ include file="common/header.jspf"%>
<div class="directionRow">
	<div>
		<%@ include file="common/navigation.jspf"%>
	</div>
	<table class="table table-striped tableData addTodo">
		<caption>
			<spring:message code="todo.caption"/>
			<a class="btn btn-success marginLeft20" href="/add-todo">Add</a>

		</caption>
		<thead>
			<tr>
				<th>Tanggal / Waktu Simpan</th>
				<th>Deskripsi</th>
				<th>Validasi</th>
				<th>Perbaharui Kategori</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${todos}" var="todo">
				<tr>
					<td><fmt:formatDate pattern="dd/MM/yyyy hh:mm:ss"
							value="${todo.targetDate}" /></td>
					<td>${todo.desc}</td>
					<td>${todo.match}</td>
					<td><a href="/update-todo?id=${todo.id}"
						class="btn btn-success">MATCH</a> <a
						href="/delete-todo?id=${todo.id}" class="btn btn-danger">UNMATCH</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<%@ include file="common/footer.jspf"%>

