<%@ include file="common/header.jspf"%>
<div class="directionRow">
	<div>
		<%@ include file="common/navigation.jspf"%>
	</div>
	<form:form method="post" commandName="todo" class="addTodo" enctype="multipart/form-data">

		<form:hidden path="id" />

		<fieldset class="form-group">	
			<form:label path="file">Upload</form:label>
			<form:input path="file" type="file" class="form-control" required="required" />
			<form:errors path="file" cssClass="text-warning" />
		</fieldset>
		<fieldset class="form-group">
			<form:label path="desc">Description</form:label>
			<form:input path="desc" type="text" class="form-control"
				required="required" />
			<form:errors path="desc" cssClass="text-warning" />
		</fieldset>

		<fieldset class="form-group">
			<form:label path="targetDate">Target Date</form:label>
			<form:input path="targetDate" type="text" class="form-control"
				required="required" />
			<form:errors path="targetDate" cssClass="text-warning" />
		</fieldset>


		<input class="btn btn-success" type="submit" value="Submit" />
	</form:form>
</div>
<%@ include file="common/footer.jspf"%>
