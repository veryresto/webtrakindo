package com.webServiceTrakindo.specimen;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import java.io.RandomAccessFile;

import com.webServiceTrakindo.exception.ExceptionController;

@Controller
@SessionAttributes("name")
public class SpecimenController {

	private Log logger = LogFactory.getLog(ExceptionController.class);

	@Autowired
	SpecimenService service;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "/list-specimen", method = RequestMethod.GET)
	public String listSpecimen(ModelMap model) {
		model.addAttribute("specimen", service.retrieveSpecimens());
		return "list-specimen";
	}

	private String retrieveLoggedinUserNameSpecimen() {
		return "webServiceTrakindo";
	}

	@RequestMapping(value = "/add-specimen", method = RequestMethod.GET)
	public String showSpecimenPage(ModelMap model) {
		String inputString = "Hello World!";
		byte[] b = inputString.getBytes();
		model.addAttribute("specimen", new Specimen(0, "", new Date(), b));
		return "specimen";

	}

	@RequestMapping(value = "/add-specimen", method = RequestMethod.POST)
	public String addSpecimen(ModelMap model, @Valid Specimen specimen, BindingResult result) {
		if (result.hasErrors()) {
			return "specimen";
		}
		        System.out.print("GfG1"); 
		String inputString = "Hello World!";
		byte[] b = inputString.getBytes();
		service.addSpecimen(specimen.getDesc(), new Date(), b);
		model.clear();
		return "redirect:list-specimen";
	}

	@ExceptionHandler(value = Exception.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		logger.error("Request " + request.getRequestURL()
				+ " Threw an Exception", ex);
		return "error-specific";
	}

}