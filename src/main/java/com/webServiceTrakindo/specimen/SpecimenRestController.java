
package com.webServiceTrakindo.specimen;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpecimenRestController {
	@Autowired
	SpecimenService service;

	@RequestMapping(value = "/specimen/{id}")
	public Specimen retrieveSpecimen(@PathVariable int id) {
		return service.retrieveSpecimen(id);
	}

}
