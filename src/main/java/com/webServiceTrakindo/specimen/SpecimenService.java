package com.webServiceTrakindo.specimen;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.io.RandomAccessFile;
import org.springframework.stereotype.Service;

@Service
public class SpecimenService {
	private static List<Specimen> specimens = new ArrayList<Specimen>();
	private static int specimenCount = 3;

	static {
	
	}

	public void addSpecimen( String desc, Date targetDate, byte[] file) { 
		specimens.add(new Specimen(++specimenCount, desc, targetDate, file));
	}
	public List<Specimen> retrieveSpecimens() {
		
		return specimens;
	}
	public void deleteSpecimen(int id) {
		Iterator<Specimen> iterator = specimens.iterator();
		while (iterator.hasNext()) {
			Specimen specimen = iterator.next();
			if (specimen.getId() == id) {
				iterator.remove();
			}
		}
	}


	public Specimen retrieveSpecimen(int id) {
		for (Specimen specimen : specimens) {
			if (specimen.getId() == id)
				return specimen;
		}
		return null;
	}

}
