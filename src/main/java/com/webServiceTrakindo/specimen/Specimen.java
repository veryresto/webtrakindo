package com.webServiceTrakindo.specimen;

import java.util.Date;

import javax.validation.constraints.Size;

public class Specimen {

	private int id;

	@Size(min = 6, message = "Enter atleast 6 characters")
	private String desc;
	private Date targetDate;


	private byte[] file;


	public Specimen() {
	}

	public Specimen(int id, String desc, Date targetDate, byte[] file) {
		super();
		this.id = id;
		this.targetDate = targetDate;
		this.desc = desc;
		this.file = file;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFile(byte[] file) {
        this.file = file;
    }

    public byte[] getFile() {
        return file;
    }
	public Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return String
				.format("ToString - Todo [id=%s, targetDate=%s, desc=%s]",
						id, getTargetDate(), desc);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Specimen other = (Specimen) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
